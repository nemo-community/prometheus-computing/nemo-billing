# Generated by Django 4.2.11 on 2024-07-26 22:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("rates", "0005_version_2_3_0"),
    ]

    operations = [
        migrations.AddField(
            model_name="rate",
            name="daily_split_multi_day_charges",
            field=models.BooleanField(
                default=False,
                help_text="Check this box to split charges spanning multiple days, leave unchecked to keep one long charge",
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="rate",
            name="daily_split_multi_day_charges",
            field=models.BooleanField(
                default=True,
                help_text="Check this box to split charges spanning multiple days, leave unchecked to keep one long charge",
            ),
        ),
    ]
